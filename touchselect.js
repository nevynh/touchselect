function touchSelect( touchSelect, heading, multiselect ){
	if( multiselect ){
		this.multiselect = true;
	}
	this.touchSelect = touchSelect;
	this.touchSelect.html(
		'<div class="container">'+
			'<div class="tab">'+
				'<div class="tabheading"></div>'+
				'<div class="tabvalue"></div>'+
			'</div>');
	if(this.multiselect){
		this.touchSelect.html(this.touchSelect.html()+
			'<div class="collapseTab">&#x25F0;</div>');
	}
	this.touchSelect.html(this.touchSelect.html()+
			'<div class="container2">'+
				'<div class="heading"></div>'+
				'<select size=10 />'+
			'</div>'+
		'</div><div class="children_container"></div>');
	this.heading = this.touchSelect.find('.heading');
	this.tab = this.touchSelect.find('.tab');
	this.select = this.touchSelect.find('select');
	this.container = this.touchSelect.find('.container2');
	this.tab.on('click', $.proxy(function(){
		this.tab.hide('blind', $.proxy(function(){
			this.container.show('blind');
		},this));
		$(this).trigger('edit');
	},this));
	this.heading.text(heading);
	this.lastSelected=null;
    this.isJSON = function (str){
        try {
            JSON.parse(str);
        }
        catch (e) {
            return false;
        }
        return true;
    };
}

touchSelect.prototype.setTab = function(heading, value){
	this.tab.find('.tabheading').html(heading+':');
	this.tab.find('.tabvalue').html(value);
};

touchSelect.prototype.populate = function(jsonData){
	if(this.isJSON(jsonData)){
		options = JSON.parse(jsonData);
	}
	else{
		options = jsonData;
	}
	count = 0;
	for(var option in options){
		this.select.html(this.select.html()+' <option id='+option+'>'+options[option]+'</option>');
		count ++;
	}
	this.select.children().on('click', $.proxy(function(){
		if( ! this.multiselect ){
			this.collapse();
		}
	}, this));
	if(count==1){
		this.select.children().first().prop('selected', true);
		this.select.children().first().trigger('click');
	}
};

touchSelect.prototype.collapse = function(){
	if(!this.multiselect){
		this.setTab(this.heading.html(), this.select.find('.selected').text());
		this.container.hide('blind', $.proxy(function(){
			this.tab.show('blind');
		},this));
		if(this.select.find(':selected').text()!==this.lastSelected){
			$(this).trigger('selected');
		}
		this.lastSelected=this.select.find(':selected').text();
	}
}

touchSelect.prototype.getSelected = function(){
	return this.select.children().filter(':selected').prop('id');
};

touchSelect.prototype.clearChildren = function(){
	this.getChildren().remove();
	this.touchSelect.append('<div class="children_container" />');
	return this.getChildren();
};

touchSelect.prototype.getChildren = function(){
	return(this.touchSelect.find('.children_container').first());
};