function touchSelect( touchSelect, heading, multiselect ){
	if( multiselect ){
		this.multiselect = true;
	}
	this.touchSelect = touchSelect;
	control_html='<div class="container">'+
			'<div class="tab">'+
				'<div class="tabheading"></div>'+
				'<div class="tabvalue"></div>'+
			'</div>'+
			'<div class="container2">';
	if(this.multiselect){
		control_html=control_html+
				'<div class="collapseTab">&#x25E4;</div>';
	}
	control_html=control_html+
				'<div class="heading"></div>'+
				'<table>'+
					'<tbody></tbody>'+
				'<table/>'+
			'</div>'+
		'</div><div class="children_container"></div>';
	this.touchSelect.append(control_html);
	if(this.multiselect){
		this.collapseTab = this.touchSelect.find('.collapseTab');
		this.collapseTab.on('click', $.proxy(function(){
			this.collapse();
			// this.collapseTab.hide();
		},this));
	}
	this.heading = this.touchSelect.find('.heading');
	this.tab = this.touchSelect.find('.tab');
	this.select = this.touchSelect.find('table tbody');
	this.container = this.touchSelect.find('.container2');
	this.tab.on('click', $.proxy(function(){
		this.tab.hide('blind', $.proxy(function(){
			this.container.show('blind');
		},this));
		$(this).trigger('edit');
	},this));
	this.heading.text(heading);
	this.lastSelected=null;
    this.isJSON = function (str){
        try {
            JSON.parse(str);
        }
        catch (e) {
            return false;
        }
        return true;
    };
}

touchSelect.prototype.setTab = function(heading, values){
	this.tab.find('.tabheading').html(heading+':');
	var val="";
	if(this.multiselect){
		values.each(function(index, obj){
			val = val + $(values[index]).text() + '<br />';
		});
		this.tab.find('.tabvalue').html(val);
	}
	else{
		this.tab.find('.tabvalue').html(values);
	}
};

touchSelect.prototype.populate = function(jsonData){
	if(this.isJSON(jsonData)){
		options = JSON.parse(jsonData);
	}
	else{
		options = jsonData;
	}
	count = 0;
	for(var option in options){
		this.select.append('<tr id='+option+'><td>'+options[option]+'</td>');
		count ++;
	}
	this.select.children().on('click', $.proxy(function(e){
		row=$(e.currentTarget);
		if( ! this.multiselect ){
			this.touchSelect.find('table tr').removeClass('selected');
			row.addClass('selected');
			this.collapse();
		}
		else{
			if(row.hasClass('selected')){
				row.removeClass('selected');
			}
			else{
				row.addClass('selected');
			}
			if(this.touchSelect.find('table .selected').length>0){
				this.collapseTab.show('blind');
			}
			else{
				this.collapseTab.hide('blind');
			}
		}
	}, this));
	if(count==1){
		this.select.children().first().prop('selected', true);
		this.select.children().first().trigger('click');
	}
};

touchSelect.prototype.getSelected = function(){
	return this.select.find('.selected');
};

touchSelect.prototype.clearChildren = function(){
	this.getChildren().remove();
	this.touchSelect.append('<div class="children_container" />');
	return this.getChildren();
};

touchSelect.prototype.getChildren = function(){
	return(this.touchSelect.find('.children_container').first());
};

touchSelect.prototype.collapse = function(){
	if(!this.multiselect){
		this.setTab(this.heading.html(), this.select.find('.selected').text());
		this.container.hide('blind', $.proxy(function(){
			this.tab.show('blind');
		},this));
		if(this.select.find(':selected').text()!==this.lastSelected){
			$(this).trigger('selected');
		}
		this.lastSelected=this.select.find(':selected').text();
	}
	else{
		this.setTab(this.heading.html(), this.select.find('.selected'));
		this.container.hide('blind', $.proxy(function(){
			this.tab.show('blind');
		},this));
	}
};