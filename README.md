# TouchSelect
## Description
TouchSelect is an attempt of a select input control more suitable for touch devices.

It starts out as an expanded select input control, and collapses, showing a heading and the value selected. To change the option, tap on the tab that the control leaves behind and change it.

Consider this a VERY early version of this control. I want to change it to be able to show columns of data though I'm completely undecided about this at the moment as my multicolumn control (I'll make a link here when I've uploaded the code) is complex.
## Dependencies
 * jquery

## Usage
### Initialize
Include both the javascript and css:
```html
<head>
    <link rel="stylesheet" href="touchselect.css" />
    <script type="text/javascript" src="touchselect.js"></script>
</head>
```
Then when you want to use it, create a div with class='touchselect':
```html
<body>
    <div class='touchselect' id='example'></div>
</body>
```
Initiate it in javascript with:
```javascript
touchselect_control = touchselect($('#example'), 'This is the heading');
```
### Populating
When you're ready to populate, you can give it either json or an array. Indexes must not include spaces. Use javascript:
```javascript
touchselect_control.populate(datea);
```
Data is in [key: value] pairs.

### Events
Upon clicking on an option, a 'selected' event is thrown. When the control's "tab" has been clicked, an "edit" event is thrown. To catch the events, you need to essentially cast the object to a jquery object. i.e.
```javascript
$(touchselect_control).on('selected', function(e){
    console.log(touchselect_control.getSelected());
});
```
### Getting selected
Get the selected item (key) via the getSelected() method.
```javascript
alert(touchselect_control.getSelected());
```